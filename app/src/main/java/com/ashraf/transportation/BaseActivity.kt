package com.ashraf.transportation

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MotionEvent

open class BaseActivity:AppCompatActivity() {
    private lateinit var touchEventManagement:TouchEventManagement
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        touchEventManagement=TouchEventManagement(this);
    }
    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        touchEventManagement.dispatchTouchEvent(ev)
        return super.dispatchTouchEvent(ev)
    }
}