package com.ashraf.transportation

import android.arch.lifecycle.MutableLiveData
import com.ashraf.transportation.network.*
import com.ashraf.transportation.network.ApiErrorHandler.getErrorData
import retrofit2.Call
import retrofit2.Response


object NetworkRequestHandler {

    fun <P:BaseResponse> handleRequest(liveData: MutableLiveData<DataWrapper<P>>, call: Call<P>){

        /*val callback=object: NetworkCallbackImpl<P>(){
            override fun onSuccess(response: P?) {
                liveData.value=response
            }

            override fun onFailure(error: ServiceErrorResponse) {
                super.onFailure(error)
                if(error!=null){
                  val param= cls.newInstance()
                   param?.serviceErrorResponse=error
                    liveData.value=param

                }

            }
        */
            val dataWrapper = DataWrapper<P>()
            val networkCallback=object:NetworkCallback<P>(){
                override fun handleResponseData(data: P) {
                    dataWrapper.data=data
                    liveData.value=dataWrapper
                }

                override fun handleError(response: Response<P>?) {
                    dataWrapper.apiException= getErrorData(response)
                    liveData.value=dataWrapper
                }

                override fun handleException(t: Exception) {
                    dataWrapper.apiException=t
                    liveData.value=dataWrapper
                }

            }
           call.enqueue(networkCallback)
        }

}