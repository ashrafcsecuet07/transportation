package com.ashraf.transportation.auth.signup

import android.app.Fragment
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ashraf.transportation.BaseFragment
import com.ashraf.transportation.R
import com.ashraf.transportation.utils.UiUtil
import kotlinx.android.synthetic.main.activity_otp_generation_sign_up.*
import kotlinx.android.synthetic.main.progress_bar.*

class OtpGenerationSignUpFragment : BaseFragment() {
    private var mSignupViewModel = SignupViewModel()
    private var sharedViewModel=SignUpStepSharedViewModel()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_otp_generation_sign_up, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        activity?.let {
            sharedViewModel = ViewModelProviders.of(activity!!).get(SignUpStepSharedViewModel::class.java)
            mSignupViewModel = ViewModelProviders.of(this).get(SignupViewModel::class.java)


        }
        mSignupViewModel.isValidMobileNumber.observe(activity!!, Observer {
            it?.let {
                til_mobile_number.error = getString(it.errorMessageResourceId)

            }
        })
        btn_sign_up.setOnClickListener {
            mSignupViewModel.mobileNumber.value = UiUtil.getTextFieldValue(et_mobile_namber)
            mSignupViewModel.generateOtp()
        }

        mSignupViewModel.isLoadingObservable.observe(this, Observer { isLoading ->
            UiUtil.setVisibility(if (isLoading!!) View.VISIBLE else View.GONE, progress_loading)

        })

        mSignupViewModel.otpGenerationObservable.observe(this, Observer {
            it?.let {
                mSignupViewModel.isLoadingObservable.value = false
                it.data?.let {
                    UiUtil.showToast(activity, it.message)
                    sharedViewModel.changeStepObservable.value=1
                } ?: UiUtil.showToast(activity, it.apiException?.message)
            }

        })

        mSignupViewModel.otpRetrievalObservable.observe(this, Observer {it?.let {
            mSignupViewModel.isLoadingObservable.value = false
            it.data?.let {
                UiUtil.showToast(activity, it.message)
                sharedViewModel.changeStepObservable.value=1
                sharedViewModel.mobileNumber.value=mSignupViewModel.mobileNumber.value
            } ?: UiUtil.showToast(activity, it.apiException?.message)

        }  })


    }


}