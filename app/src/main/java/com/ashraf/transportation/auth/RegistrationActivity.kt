package com.ashraf.transportation.auth

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.design.widget.TabLayout.OnTabSelectedListener
import android.support.v4.app.Fragment
import com.ashraf.transportation.BaseActivity
import com.ashraf.transportation.R
import com.ashraf.transportation.auth.signup.*
import kotlinx.android.synthetic.main.activity_registration.*


class RegistrationActivity :BaseActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        setupTab()
        setupViewModel()
    }

   private fun setupTab(){
        supportFragmentManager.beginTransaction().replace(R.id.registration_container,OtpGenerationSignUpFragment()).commit()
        val tabSelectionListener=  object:OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                var fragment:Fragment?=null
                if (tab?.position==0)
                fragment=OtpGenerationSignUpFragment()
                else if (tab?.position==1)
                    fragment= OtpVerificationFragment()
                else if (tab?.position==2)
                    fragment=SignUpInfoAddFragment()
                else if (tab?.position==3)
                    fragment=NationalIDInfoFragment()

                fragment?.let {
                    supportFragmentManager.beginTransaction().replace(R.id.registration_container,fragment).commit()
                }
            }
        }
        tabLayout.addOnTabSelectedListener(tabSelectionListener)

    }

   private fun setupViewModel(){
       val sharedViewModel=ViewModelProviders.of(this).get(SignUpStepSharedViewModel::class.java)
       sharedViewModel.changeStepObservable.observe(this, Observer {it?.let { tabLayout.getTabAt(it)?.select() }
       })
   }

}
