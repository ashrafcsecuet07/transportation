package com.ashraf.transportation.auth

import com.ashraf.transportation.BaseResponse

class OTPRetrievalResponse : BaseResponse() {
    var data=OtpFeedback()
}

class OtpFeedback {
    var otp = ""
    var sentStatus = ""
    var otpExpiryDate = ""
    var mobileNumber = ""
    var otpStatus = ""

}