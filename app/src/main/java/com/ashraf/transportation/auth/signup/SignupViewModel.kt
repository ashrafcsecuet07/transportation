package com.ashraf.transportation.auth.signup

import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.ashraf.transportation.FormValidation
import com.ashraf.transportation.LoadingViewModel
import com.ashraf.transportation.R
import com.ashraf.transportation.SingleLiveEvent
import com.ashraf.transportation.auth.*
import com.ashraf.transportation.auth.login.LoginResponse
import com.ashraf.transportation.network.DataWrapper

class SignupViewModel: LoadingViewModel {
    val otpGenerationObservable: MutableLiveData<DataWrapper<OtpGenerationResponse>>
    val otpRetrievalObservable: MutableLiveData<DataWrapper<OTPRetrievalResponse>>
    val otpVerificationObservable= MutableLiveData<DataWrapper<OtpVerificationResponse>>()
    val isValidMobileNumber = MutableLiveData<FormValidation>()
    val isValidOtp= MutableLiveData<FormValidation>()
    val isValidPassword= MutableLiveData<FormValidation>()


    val mobileNumber = MutableLiveData<String>()
    val otp = MutableLiveData<String>()


    constructor() {
        otpGenerationObservable = MutableLiveData()
        otpRetrievalObservable = MutableLiveData()
    }


    fun generateOtp() {
        if (mobileNumber.value.isNullOrEmpty()) {
            isValidMobileNumber.value = FormValidation(R.string.mobile_number_required)
        } else {
            isLoadingObservable.value = true
            // AuthRepository.generateOtp(otpGenerationObservable, mobileNumber.value!!)
            AuthRepository.getOtp(otpRetrievalObservable, mobileNumber.value!!)
        }
    }

    fun verifyOTP() {
        if(otp.value.isNullOrEmpty())
            isValidOtp.value= FormValidation(R.string.field_is_required)
        else {
            isLoadingObservable.value = true
            AuthRepository.verifyOtp(otpVerificationObservable, otp.value!!,mobileNumber.value!!)
        }
    }

    fun validateUserCredential(password:String){
        if(password.isNullOrEmpty()){
            isValidPassword.value=FormValidation(R.string.field_is_required)
        }else
            isValidPassword.value=FormValidation()

    }
}