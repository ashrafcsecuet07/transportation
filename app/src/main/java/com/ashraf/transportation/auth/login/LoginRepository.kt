package com.ashraf.transportation.auth

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.ashraf.transportation.NetworkRequestHandler
import com.ashraf.transportation.auth.login.LoginRequest
import com.ashraf.transportation.auth.login.LoginResponse
import com.ashraf.transportation.auth.login.LoginServiceApi
import com.ashraf.transportation.network.*
import retrofit2.Call

object  LoginRepository {
    val api:LoginServiceApi

    init {
        api=RestClient.generateService(LoginServiceApi::class.java)
    }


     fun makeLogin(data:MutableLiveData<DataWrapper<LoginResponse>>,request: LoginRequest) {
         val call= api.login(request)
         NetworkRequestHandler.handleRequest(data,call)

         /* val callback=object:NetworkCallbackImpl<LoginResponse>(){
              override fun onSuccess(response: LoginResponse?) {
                  data.value=response
              }

              override fun onFailure(error: ServiceErrorResponse) {
                  super.onFailure(error)
                  if(error!=null){
                    data.value= LoginResponse(error)

                  }
              }

          }
          processCallBack(api.login(request),callback)
     */
     }



     fun saveLoginData(response: LoginResponse) {
    }


}
