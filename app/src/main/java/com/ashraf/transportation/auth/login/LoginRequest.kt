package com.ashraf.transportation.auth.login

data class LoginRequest(var mobileNum: String = "",
                        var password: String = "",
                        var roleId: Int =4 )