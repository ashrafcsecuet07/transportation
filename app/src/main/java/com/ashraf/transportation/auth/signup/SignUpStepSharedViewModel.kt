package com.ashraf.transportation.auth.signup

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel

class SignUpStepSharedViewModel:ViewModel() {
    val changeStepObservable=MutableLiveData<Int>()
    val mobileNumber = MutableLiveData<String>()
    val password = MutableLiveData<String>()
    val email = MutableLiveData<String>()
    val referrel = MutableLiveData<String>()

}