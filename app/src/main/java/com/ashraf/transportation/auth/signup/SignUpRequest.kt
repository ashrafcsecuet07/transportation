package com.ashraf.transportation.auth.signup

import com.google.gson.annotations.SerializedName

class SignUpRequest {

    @SerializedName("dob")
    var dob: String? = null
    @SerializedName("driverBackPhoto")
    var driverBackPhoto: String? = null
    @SerializedName("driverFrontPhoto")
    var driverFrontPhoto: String? = null
    @SerializedName("driverLicenseExpiryDate")
    var driverLicenseExpiryDate: String? = null
    @SerializedName("driverLicenseNumber")
    var driverLicenseNumber: String? = null
    @SerializedName("email")
    var email: String? = null
    @SerializedName("fleetOwnerDetails")
    var fleetOwnerDetails: Long? = null
    @SerializedName("location")
    var location: String? = null
    @SerializedName("masterDistrictId")
    var masterDistrictId: Long? = null
    @SerializedName("mobileNumber")
    var mobileNumber: String? = null
    @SerializedName("name")
    var name: String? = null
    @SerializedName("nationalId")
    var nationalId: String? = null
    @SerializedName("nationalIdBackPhoto")
    var nationalIdBackPhoto: String? = null
    @SerializedName("nationalIdFrontPhoto")
    var nationalIdFrontPhoto: String? = null
    @SerializedName("password")
    var password: String? = null
    @SerializedName("referrelCode")
    var referrelCode: String? = null
    @SerializedName("roleId")
    var roleId: Long? = null
    constructor()


}
