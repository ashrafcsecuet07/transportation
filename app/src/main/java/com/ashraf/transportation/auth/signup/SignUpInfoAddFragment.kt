package com.ashraf.transportation.auth.signup

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.constraint.solver.widgets.ConstraintAnchor
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ashraf.transportation.BaseFragment
import com.ashraf.transportation.FormValidation
import com.ashraf.transportation.R
import com.ashraf.transportation.utils.UiUtil
import kotlinx.android.synthetic.main.fragment_signup_info.*

class SignUpInfoAddFragment: BaseFragment() {
    private var mSignupViewModel = SignupViewModel()
    private var sharedViewModel=SignUpStepSharedViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_signup_info,container,false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        activity?.let {
            sharedViewModel = ViewModelProviders.of(activity!!).get(SignUpStepSharedViewModel::class.java)
            mSignupViewModel = ViewModelProviders.of(this).get(SignupViewModel::class.java)

        }

        btn_submit.setOnClickListener{
            mSignupViewModel.validateUserCredential(UiUtil.getTextFieldValue(et_password))
        }

        mSignupViewModel.isValidPassword.observe(this, Observer {  it?.let {
            if(it.isValid){
                sharedViewModel.email.value=UiUtil.getTextFieldValue(et_email)
                sharedViewModel.referrel.value=UiUtil.getTextFieldValue(et_referral)
                sharedViewModel.changeStepObservable.value=3
            }else{
                til_password.error=getString(it.errorMessageResourceId)
            }

        } })
    }
}