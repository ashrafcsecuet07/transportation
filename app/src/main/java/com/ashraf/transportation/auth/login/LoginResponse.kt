package com.ashraf.transportation.auth.login

import com.ashraf.transportation.BaseResponse
import com.ashraf.transportation.network.ServiceErrorResponse

open class LoginResponse : BaseResponse {
    var data= LoginFeedback()
    constructor():super()

}
