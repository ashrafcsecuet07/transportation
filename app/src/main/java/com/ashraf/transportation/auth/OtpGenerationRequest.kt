package com.ashraf.transportation.auth

data class OtpGenerationRequest (
    val otp:Int=0,
    val roleId:Int=4,
    val mobileNumber:String=""
)
