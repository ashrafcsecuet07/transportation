package com.ashraf.transportation.auth.login

import android.arch.lifecycle.LiveData

interface LoginDataSource {

    fun makeLogin(request: LoginRequest):LiveData<LoginResponse>;
    fun saveLoginData(response: LoginResponse);

}