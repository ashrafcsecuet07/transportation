package com.ashraf.transportation.auth.login

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginServiceApi {

    @POST("auth/signIn")
    fun login( @Body request: LoginRequest): Call<LoginResponse>
}
