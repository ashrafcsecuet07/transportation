package com.ashraf.transportation.auth.signup

import retrofit2.http.Body
import retrofit2.http.POST

interface SignupService {
    @POST("auth/signup")
    fun makeSignUp(@Body request:SignUpRequest)



}