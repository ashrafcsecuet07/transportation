package com.ashraf.transportation.auth.signup

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ashraf.transportation.BaseFragment
import com.ashraf.transportation.R
import android.content.ActivityNotFoundException
import android.content.Intent
import android.app.Activity
import android.graphics.Bitmap
import kotlinx.android.synthetic.main.fragment_national_id_info.*
import android.graphics.BitmapFactory
import android.util.Base64
import android.util.Log
import com.ashraf.transportation.utils.UiUtil
import com.squareup.picasso.Picasso
import java.io.ByteArrayOutputStream


class NationalIDInfoFragment:BaseFragment() {
    private var mSignupViewModel = SignupViewModel()
    private var sharedViewModel=SignUpStepSharedViewModel()
    private val REQUESTCODE_PICK_IMAGE_FRONT=100
    private val REQUESTCODE_PICK_IMAGE_BACK=101
    private var frontImageEncodeVal=""
    private var backImageEncodeVal=""


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_national_id_info,container,false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.let {
            sharedViewModel = ViewModelProviders.of(activity!!).get(SignUpStepSharedViewModel::class.java)
            mSignupViewModel = ViewModelProviders.of(this).get(SignupViewModel::class.java)


        }
        tv_nid_front.setOnClickListener{
            chooseFile(REQUESTCODE_PICK_IMAGE_FRONT)
        }
        tv_nid_back.setOnClickListener{
            chooseFile(REQUESTCODE_PICK_IMAGE_BACK)
        }


    }

    private fun chooseFile(requestCode:Int) {
        try {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            try {
                startActivityForResult(intent, requestCode)

            } catch (e: ActivityNotFoundException) {

            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK)
        data?.let {
            val imageUri = data.data
            val imageStream = activity?.contentResolver?.openInputStream(imageUri)
            val selectedImage = BitmapFactory.decodeStream(imageStream)
            val encodedImage = encodeImage(selectedImage)
             Log.d("encodeString",encodedImage)
            if(requestCode==REQUESTCODE_PICK_IMAGE_FRONT){
                Picasso.get().load(imageUri).into(iv_nid_front)
                frontImageEncodeVal=encodedImage
                UiUtil.setVisibility(View.VISIBLE,iv_nid_front)

            }else if(requestCode==REQUESTCODE_PICK_IMAGE_BACK){
                Picasso.get().load(imageUri).into(iv_nid_back)
                backImageEncodeVal=encodedImage
                UiUtil.setVisibility(View.VISIBLE,iv_nid_back)
            }

        }
    }

    private fun encodeImage(bm: Bitmap): String {
        val baos = ByteArrayOutputStream()
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val b = baos.toByteArray()
        return android.util.Base64.encodeToString(b, Base64.DEFAULT)
    }


}