package com.ashraf.transportation.auth

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.MutableLiveData
import com.ashraf.transportation.FormValidation
import com.ashraf.transportation.LoadingViewModel
import com.ashraf.transportation.SingleLiveEvent
import com.ashraf.transportation.auth.login.LoginRequest
import com.ashraf.transportation.auth.login.LoginResponse
import com.ashraf.transportation.network.DataWrapper

class LoginViewModel : LoadingViewModel{
     val loginResponseObservable:MutableLiveData<DataWrapper<LoginResponse>>
     val submitButtonClick = SingleLiveEvent<Any>()
     val isValidUserName=MediatorLiveData<FormValidation>()
     val isValidPassword=MediatorLiveData<FormValidation>()
     val onValidated=MediatorLiveData<Boolean>()
     var mLoginRequest=LoginRequest()


    constructor(){
        loginResponseObservable=MutableLiveData<DataWrapper<LoginResponse>>()
    }


    fun onSubmitButtonClick(){
        submitButtonClick.call()
    }

    fun validate(){
        if(mLoginRequest.mobileNum.isNullOrEmpty()){
            val validation=FormValidation()
            validation.errorMessage="Mobile Number is Empty"
            isValidUserName.value=validation

        }
        else if(mLoginRequest.password.isNullOrEmpty()){
            val validation=FormValidation()
            validation.errorMessage="Password is Empty"
            isValidPassword.value=validation
        }else
            onValidated.value=true


    }

    fun login(){
        isLoadingObservable.value=true
        LoginRepository.makeLogin(loginResponseObservable,mLoginRequest)

    }



}
