package com.ashraf.transportation.auth

import com.ashraf.transportation.BaseResponse
import retrofit2.Call
import retrofit2.http.*

interface AuthApiService {
    @POST("auth/generateOtp")
    fun generateOtp(@Body request: OtpGenerationRequest):Call<OtpGenerationResponse>

    @POST("auth/verifyOtp")
    fun verifyOtp(@Body request: OtpGenerationRequest):Call<OtpVerificationResponse>

    @POST("auth/validateEmail")
    fun validateEmail(@Body request: EmailValidationRequest):Call<BaseResponse>

    @GET("auth/getOtp")
    fun getOtp(@Query("mobileNumber") mobileNumber:String ):Call<OTPRetrievalResponse>

}