package com.ashraf.transportation.auth.signup

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.View
import com.ashraf.transportation.BaseActivity
import com.ashraf.transportation.R
import com.ashraf.transportation.utils.UiUtil
import kotlinx.android.synthetic.main.activity_otp_generation_sign_up.*
import kotlinx.android.synthetic.main.progress_bar.*
import okhttp3.internal.Util

class OtpGenerationSignUpActivity:BaseActivity(){
    private var mSignupViewModel= SignupViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp_generation_sign_up)
    }
}
