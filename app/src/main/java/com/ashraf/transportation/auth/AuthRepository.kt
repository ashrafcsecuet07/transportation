package com.ashraf.transportation.auth

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.ashraf.transportation.BaseResponse
import com.ashraf.transportation.NetworkRequestHandler
import com.ashraf.transportation.auth.login.LoginServiceApi
import com.ashraf.transportation.auth.signup.SignUpRequest
import com.ashraf.transportation.network.DataWrapper
import com.ashraf.transportation.network.RestClient
import com.squareup.picasso.RequestHandler

object AuthRepository {
    val api: AuthApiService

    init {
        api= RestClient.generateService(AuthApiService::class.java)
    }

    fun generateOtp(otpGenerationResponse: MutableLiveData<DataWrapper<OtpGenerationResponse>>, mobileNumber:String){
        val call=api.generateOtp(OtpGenerationRequest(mobileNumber=mobileNumber))
        NetworkRequestHandler.handleRequest(otpGenerationResponse,call)
    }

    fun getOtp(response: MutableLiveData<DataWrapper<OTPRetrievalResponse>>, mobileNumber:String){
        val call=api.getOtp(mobileNumber)
        NetworkRequestHandler.handleRequest(response,call)
    }

    fun verifyOtp(response: MutableLiveData<DataWrapper<OtpVerificationResponse>>, otp:String,mobileNumber: String){
        val call=api.verifyOtp(OtpGenerationRequest(otp=otp.toInt(),mobileNumber = mobileNumber))
        NetworkRequestHandler.handleRequest(response,call)
    }
    fun validateEmail(response: MutableLiveData<DataWrapper<BaseResponse>>, email:String){
        val call=api.validateEmail(EmailValidationRequest(email))
        NetworkRequestHandler.handleRequest(response,call)
    }


}