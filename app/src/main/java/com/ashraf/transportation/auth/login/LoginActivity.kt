package com.ashraf.transportation.auth.login

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import com.ashraf.transportation.BaseActivity
import com.ashraf.transportation.R
import com.ashraf.transportation.auth.LoginViewModel
import com.ashraf.transportation.auth.RegistrationActivity
import com.ashraf.transportation.auth.signup.OtpGenerationSignUpActivity
import com.ashraf.transportation.profile.ProfileAcitvty
import com.ashraf.transportation.utils.*
import com.binjar.prefsdroid.Preference
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.progress_bar.*

class LoginActivity : BaseActivity() {
    private var mLoginViewModel= LoginViewModel()
    val mLoginRequest: LoginRequest = LoginRequest()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        mLoginViewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)


        btn_submit.setOnClickListener {
            mLoginRequest.mobileNum=UiUtil.getTextFieldValue(et_mobile_number)
            mLoginRequest.password=UiUtil.getTextFieldValue(et_password)
            mLoginRequest.roleId=if(rb_agent.isChecked) 4 else 3
            mLoginViewModel.mLoginRequest=mLoginRequest
            mLoginViewModel.onSubmitButtonClick()
        }

        tv_sign_up.setOnClickListener{
            NavigationTransitionUtil.gotoNewActivity(this,RegistrationActivity::class.java)
        }

        mLoginViewModel.submitButtonClick.observe(this, Observer {
            mLoginViewModel.validate()

        })

        mLoginViewModel.isValidUserName.observe(this, Observer {item->
            if (item != null) {
                FormValidationUtils.showErrorMessage(til_mobile_number,item.errorMessage)
            }

        })

        mLoginViewModel.isValidPassword.observe(this, Observer {item->
            if (item != null) {
                FormValidationUtils.showErrorMessage(til_password,item.errorMessage)
            }
        })

        mLoginViewModel.isLoadingObservable.observe(this, Observer { isLoading->
            UiUtil.setVisibility( if(isLoading!!)View.VISIBLE else View.GONE,progress_loading)

        })

        mLoginViewModel.onValidated.observe(this, Observer { isValidated->isValidated?.let {
            if (isValidated && NetworkUtils.isNetworkAvailable(this)) {
                UiUtil.disbleTouch(this)
                mLoginViewModel.login()
            }
        }

        })

        mLoginViewModel.loginResponseObservable.observe(this,Observer{
            response->
            mLoginViewModel.isLoadingObservable.value=false
            UiUtil.enableTouch(this)
            response?.let {
                response.data?.let {it->
                    UiUtil.showToast(this, it.message)
                    Preference.putString("token",it.data.token)
                    Preference.putInt("role",mLoginRequest.roleId)
                    NavigationTransitionUtil.gotoNewActivity(this,ProfileAcitvty::class.java)

                }?:kotlin.run {
                    UiUtil.showToast(this, response.apiException!!.message);

                }


            }


        })
    }



}
