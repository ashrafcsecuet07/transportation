package com.ashraf.transportation.auth.signup

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ashraf.transportation.R
import com.ashraf.transportation.utils.UiUtil
import kotlinx.android.synthetic.main.fragment_otp_verification.*
import kotlinx.android.synthetic.main.progress_bar.*

class OtpVerificationFragment:Fragment() {
    private var mSignupViewModel = SignupViewModel()
    private var sharedViewModel=SignUpStepSharedViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_otp_verification,container,false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.let {
            sharedViewModel = ViewModelProviders.of(activity!!).get(SignUpStepSharedViewModel::class.java)
            mSignupViewModel = ViewModelProviders.of(this).get(SignupViewModel::class.java)

        }
        mSignupViewModel.isLoadingObservable.observe(this, Observer {isLoading ->
            UiUtil.setVisibility(if (isLoading!!) View.VISIBLE else View.GONE, progress_loading)

        })

        mSignupViewModel.isValidOtp.observe(this, Observer {  it?.let {
            til_otp.error = getString(it.errorMessageResourceId)

        } })
        mSignupViewModel.otpVerificationObservable.observe(this, Observer {it?.let {

            mSignupViewModel.isLoadingObservable.value = false
            it.data?.let {
                UiUtil.showToast(activity, it.message)
                sharedViewModel.changeStepObservable.value=2
            } ?: UiUtil.showToast(activity, it.apiException?.message)

        }  })



        btn_submit.setOnClickListener{
            mSignupViewModel.otp.value = UiUtil.getTextFieldValue(et_otp)
            mSignupViewModel.mobileNumber.value=sharedViewModel.mobileNumber.value
            mSignupViewModel.verifyOTP()
        }


    }
}