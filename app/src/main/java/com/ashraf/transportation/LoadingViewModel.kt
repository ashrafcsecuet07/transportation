package com.ashraf.transportation

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.MutableLiveData



open class LoadingViewModel:ViewModel {
    val isLoadingObservable = MutableLiveData<Boolean>()
    constructor(){}
}