package com.ashraf.transportation;

import android.arch.lifecycle.MutableLiveData;

import com.ashraf.transportation.network.NetworkCallbackImpl;
import com.ashraf.transportation.network.ServiceErrorResponse;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import retrofit2.Call;

public class ServiceRequestHandler<P extends BaseResponse> {

    public <P extends BaseResponse> void  handleRequest(MutableLiveData<P> data, Call<P>call) {

        NetworkCallbackImpl<P> callback = new NetworkCallbackImpl<P>() {
            @Override
            public void onSuccess(@Nullable P response) {
                data.setValue(response);

            }

            @Override
            public void onFailure(@NotNull ServiceErrorResponse error) {
                super.onFailure(error);
            }


        };
    }

        interface MyFactory<T> {
            T newObject();
        }

        class MyClass<T> {
            T field;

            public void myMethod(MyFactory<T> factory) {
                field = factory.newObject();
            }
        }
    }

    /* fun <P:BaseResponse> handleRequest(data: MutableLiveData<P>, call: Call<P>){
        val callback=object: NetworkCallbackImpl<P>(){
            override fun onSuccess(response: P?) {
                data.value=response
            }

            override fun onFailure(error: ServiceErrorResponse) {
                super.onFailure(error)
                if(error!=null){
                    val param= getValue<P>()
                    param?.serviceErrorResponse=error
                    data.value=param

                }

            }

        }

        processCallBack(call,callback)
        fun <T> processCallBack(call: Call<T>, callback: NetworkCallbackListener<T>) {
        call.enqueue(NetworkCallback<T>( callback))
    }
    }*/



