package com.ashraf.transportation.network

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import com.google.gson.Gson
import java.io.IOException
import com.ashraf.transportation.BaseResponse
import com.ashraf.transportation.R
import com.ashraf.transportation.utils.UiUtil.getResourceString
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.icu.lang.UCharacter.GraphemeClusterBreak.T






abstract class NetworkCallback<T> : Callback<T> {
    var callback:NetworkCallbackListener<T>?=null


    override fun onResponse(call: Call<T>, response: Response<T>?) {
        response?.let {
            response.body()?.let {
                handleResponseData(response.body())
            } ?: kotlin.run {
                handleError(response)
            }

        }


            }

    override fun onFailure(call: Call<T>, t: Throwable) {
        callback?.onFailure(ServiceErrorResponse(t))
        if (t is Exception) {
            handleException(t)
        } else {
            //do something else
        }

    }

    protected abstract fun handleResponseData(data: T)

    protected abstract fun handleError(response: Response<T>?)

    protected abstract fun handleException(t: Exception)


}
