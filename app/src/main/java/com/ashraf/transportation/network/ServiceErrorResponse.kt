package com.ashraf.transportation.network

import com.ashraf.transportation.BaseResponse
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class ServiceErrorResponse {

    var baseResponse:BaseResponse?=null
    private var throwable: Throwable? = null

    constructor()
    constructor(throwable: Throwable) {
        this.throwable = throwable
        var errorMessage = "Something going wrong,try again later"
        if (throwable != null) {
            if (throwable is SocketTimeoutException)
                errorMessage ="Connection Timeout, try again later"
            else if (throwable is ConnectException)
                errorMessage = "Unable to Connect"
            else if (throwable is UnknownHostException)
                errorMessage ="Requested Service is Unavailable"
        }
        baseResponse= BaseResponse()
        baseResponse!!.message=errorMessage;
    }

    fun getThrowable(): Throwable? {
        return throwable
    }

    fun setThrowable(throwable: Throwable) {
        this.throwable = throwable
    }

}