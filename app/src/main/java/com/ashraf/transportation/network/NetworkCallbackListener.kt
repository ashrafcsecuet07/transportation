package com.ashraf.transportation.network

open interface NetworkCallbackListener<Item> {
    fun onSuccess(response: Item?)
    fun onFailure(error: ServiceErrorResponse)
    fun onUnRecognizedResponse(response: Item)
}