package com.ashraf.transportation.network;

import android.os.Build;
import android.util.Log;

import com.ashraf.transportation.BuildConfig;
import com.ashraf.transportation.utils.NetworkUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;

import okhttp3.CacheControl;
import okhttp3.ConnectionSpec;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.TlsVersion;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {

    private static Retrofit retrofit;

    public  static <Service> Service generateService(Class<Service> serviceClass) {
        if (null == retrofit) {
            synchronized (Retrofit.class){
                if (null == retrofit) {
                    retrofit =getRetrofit();
                }
            }
        }

        return retrofit.create(serviceClass);
    }

    public void onDestroyRetrofit(){
        retrofit=null;
    }



    public static void callApi()
    {

    }

    public static Retrofit getRetrofit() {
        /*Gson gson = new GsonBuilder()
                .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
                .create();*/
        Gson gson = new GsonBuilder().create();
        //  Gson gson = new GsonBuilder().serializeNulls().create();
        OkHttpClient okHttpClient=getOkhttp();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://staging.ejogajog.com/ejogajog/api/v1/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();

        return retrofit;
        // apiService = retrofit.create(ApiService.class);
    }

    public static OkHttpClient getOkhttp()
    {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        if(BuildConfig.DEBUG) {
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        }else
            interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
       /* Context context= MyApplication.currentLocale
        File httpCacheDirectory = new File(getCacheDir(), "responses");
        int cacheSize = 10*1024*1024;
        Cache cache = new Cache(httpCacheDirectory, cacheSize);*/


        OkHttpClient.Builder okhttpBuilder = new OkHttpClient.Builder();
        okhttpBuilder.readTimeout(80, TimeUnit.SECONDS);
        okhttpBuilder.writeTimeout(80, TimeUnit.SECONDS);
        okhttpBuilder.addInterceptor(interceptor);
        okhttpBuilder.addInterceptor(getInterceptor());
        OkHttpClient client = okhttpBuilder.build();
        return client ;


    }



    private static Interceptor getInterceptor() {
        return new Interceptor() {


            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder builder=original.newBuilder();
                builder.cacheControl(new CacheControl.Builder()
                        .maxAge(0, TimeUnit.SECONDS).build());
                Request request = builder
                        .addHeader("Content-Type","application/json")
                        .addHeader("Accept","application/json")
                        .addHeader("authtoken", NetworkUtils.INSTANCE.getAuthToken())
                        .method(original.method(), original.body())
                        .build();
                long t1 = System.nanoTime();
                Response response= chain.proceed(request);
                long t2 = System.nanoTime();
                Log.v("ResponseTime:",""+((t2 - t1) / 1e6d));
                return response;
            }


        };
    }
    /*private static HttpUrl getHttpUrl( Request.Builder builder){

        HttpUrl originalHttpUrl = builder.build().url();
        HttpUrl url = originalHttpUrl.newBuilder()
                .addQueryParameter("lang", SessionManagement.language)
                .build();
        builder.url(url);
        return url;
    }*/
}

