package com.ashraf.transportation.network

import com.ashraf.transportation.BaseResponse
import com.google.gson.Gson
import retrofit2.Response
import java.io.IOException
import java.lang.Exception
import org.json.JSONObject


object ApiErrorHandler{
    fun <T>getErrorData(response: Response<T>?):Exception {
        response?.let {
            if (response.code() >= 400 && response.code() < 500) {
                try {
                    if (response.errorBody() != null) {
                        val jObjError = JSONObject(response.errorBody().string())
                        return Exception(jObjError.getString("message"))

                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                    return e
                }

            } else if ((response.code() >= 500 && response.code() < 600) || response == null) {
                return Exception("Service is unavailable")

            } else
                return Exception("Unknown Exception")
        }
        return Exception("Unknown Exception")
    }



}
