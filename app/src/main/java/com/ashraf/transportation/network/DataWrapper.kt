package com.ashraf.transportation.network

import java.lang.Exception

class DataWrapper<T> {
    var data:T? = null
    var apiException:Exception ?=null
}