package com.ashraf.transportation.network

abstract class NetworkCallbackImpl<T>:NetworkCallbackListener<T> {
    override fun onFailure(error: ServiceErrorResponse) {}
    override fun onUnRecognizedResponse(unrecognizedErrorResponse: T) {}
}