package com.ashraf.transportation.network

import android.content.Context
import android.net.ConnectivityManager
import com.ashraf.transportation.MyApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(val app: MyApplication) {

    @Provides
    @Singleton
    fun app(): MyApplication {
        return app
    }

    @Provides
    @Singleton
    fun context(): Context {
        return app
    }

    @Provides
    @Singleton
    fun connectivity(): ConnectivityManager {
        val service = app.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return service
    }
}