package com.ashraf.transportation.network

import com.ashraf.transportation.MyApplication
import dagger.Component
import javax.inject.Singleton

interface AppComponent {
    @Singleton
    @Component(
            modules = arrayOf(AppModule::class)
    )
    interface AppComponent {
        fun inject(app: MyApplication)
    }
}