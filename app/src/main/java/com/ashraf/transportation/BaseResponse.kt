package com.ashraf.transportation

import com.ashraf.transportation.network.ServiceErrorResponse

open class BaseResponse  {
    var serviceErrorResponse:ServiceErrorResponse?=null
    set(value) {
        field=value
        serviceErrorResponse?.let {
            message= serviceErrorResponse!!.baseResponse!!.message;
        }
    }
    var success=false
    var message=""
    var responseCode=0
    constructor()

}