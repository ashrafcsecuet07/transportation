package com.ashraf.transportation

import android.app.Application
import android.net.ConnectivityManager
import android.util.Log
import com.ashraf.transportation.network.AppComponent
import com.ashraf.transportation.network.AppModule
import com.binjar.prefsdroid.Preference
import javax.inject.Inject
class MyApplication:Application() {
    override fun onCreate() {
        super.onCreate()
        Preference.load().using(this).prepare();
    }
}