package com.ashraf.transportation

import android.graphics.Rect
import android.support.v7.app.AppCompatActivity
import android.view.MotionEvent
import android.view.View
import android.widget.EditText
import com.ashraf.transportation.utils.KeyboardManagementUtil

class TouchEventManagement  {

        var context: AppCompatActivity? = null
        var nonEffectableViews: List<View> = mutableListOf()

        constructor(appCompatActivity: AppCompatActivity) {
            this.context = appCompatActivity

        }



        fun dispatchTouchEvent(event: MotionEvent?) {
            if (event?.getAction() == MotionEvent.ACTION_DOWN) {
                val v = context?.getCurrentFocus()
                if (v is EditText) {
                    clearEdittextFocus(v, event)
                }
            }
            KeyboardManagementUtil.hideSoftKeyboard(context)
        }

        protected fun clearEdittextFocus(v: View, event: MotionEvent?) {

                 v.clearFocus()
                KeyboardManagementUtil.hideSoftKeyboard(context)

        }




       /* protected fun onButtonTouched(event: MotionEvent): Boolean {
            return if (isToucheEventInViewBoundary( event)) true else false

        }

    protected fun isToucheEventInViewBoundary( event: MotionEvent): Boolean {
        if (nonEffectableViews != null) {
            val outRect = Rect()
            for(view in nonEffectableViews!!) {
                view?.getGlobalVisibleRect(outRect)
                if( outRect.contains(event.rawX.toInt(), event.rawY.toInt()))
                    return true
            }
            return false
        } else
            return false
    }*/
    }

