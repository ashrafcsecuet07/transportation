package com.ashraf.transportation.profile

import android.arch.lifecycle.MutableLiveData
import com.ashraf.transportation.LoadingViewModel
import com.ashraf.transportation.network.DataWrapper

class ProfileViewModel :LoadingViewModel {
    val profleResponse:MutableLiveData<DataWrapper<ProfleResponse>>
    constructor(){
        profleResponse=MutableLiveData()
        isLoadingObservable.value=true
        ProfileRepository.loadProfileData(profleResponse)
    }


}