package com.ashraf.transportation.profile

import android.arch.lifecycle.MutableLiveData
import com.ashraf.transportation.NetworkRequestHandler
import com.ashraf.transportation.auth.LoginRepository
import com.ashraf.transportation.auth.login.LoginResponse
import com.ashraf.transportation.network.*
import com.binjar.prefsdroid.Preference
import retrofit2.Call

object ProfileRepository  {
    val api: ProfileApiServce

    init {
        api= RestClient.generateService(ProfileApiServce::class.java)
    }

    fun loadProfileData(data: MutableLiveData<DataWrapper<ProfleResponse>>){

        val call= api.loadProfileData(Preference.getInt("role").toString())
        NetworkRequestHandler.handleRequest(data,call)

    }






}