package com.ashraf.transportation.profile

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ProfileApiServce {
    @GET("user/profile/{role}")
    fun loadProfileData( @Path("role") role:String): Call<ProfleResponse>
}