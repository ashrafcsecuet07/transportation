package com.ashraf.transportation.profile

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.ashraf.transportation.R
import com.ashraf.transportation.utils.NetworkUtils
import com.ashraf.transportation.utils.UiUtil
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileAcitvty : AppCompatActivity() {
    private var mProfileViewModel = ProfileViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        val factory = ViewModelProvider.AndroidViewModelFactory.getInstance(application)
        mProfileViewModel = ViewModelProviders.of(this, factory).get(ProfileViewModel::class.java)
        mProfileViewModel.isLoadingObservable.observe(this, Observer { isLoading ->
            UiUtil.setVisibility(if (isLoading!!) View.VISIBLE else View.GONE, progress_loading)
        })
        mProfileViewModel.profleResponse.observe(this, Observer { response ->
            mProfileViewModel.isLoadingObservable.value=false
            response?.let {it->

                it.data?.let {

                    if(!it.data.nationalIdFrontPhoto.isNullOrEmpty())
                    Picasso.get().load(it.data.nationalIdFrontPhoto).into(profile_image)
                    tv_name.setText(it.data.name)
                    tv_role_type.setText(it.data.logInAs)
                    tv_email.setText(it.data.email)
                }?:kotlin.run {
                    UiUtil.showToast(this,it.apiException?.message)
                }
            }
        })
    }
}