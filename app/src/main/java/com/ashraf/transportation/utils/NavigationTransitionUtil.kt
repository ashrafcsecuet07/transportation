package com.ashraf.transportation.utils

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.ashraf.transportation.R

object NavigationTransitionUtil {
    fun gotoNewActivity(sourceActivity: Activity, destinationActivity: Class<*>, bundle: Bundle?=null) {


            if (sourceActivity.javaClass != destinationActivity) {
                val intent = Intent(sourceActivity, destinationActivity)
                if(bundle!=null)
                    intent.putExtras(bundle)
                sourceActivity.startActivity(intent)
                setActvitytTransitionAnimation(sourceActivity)

            }

        }
        fun setActvitytTransitionAnimation(activity:Activity){
            activity.overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit)
        }

    fun makefragmentTransition(){

    }
}