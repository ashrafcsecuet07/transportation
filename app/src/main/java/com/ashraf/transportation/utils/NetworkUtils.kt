package com.ashraf.transportation.utils

import android.content.Context
import android.net.ConnectivityManager
import com.ashraf.transportation.BaseResponse
import com.ashraf.transportation.network.ResponseCode
import com.binjar.prefsdroid.Preference

object NetworkUtils {
     fun isNetworkAvailable(context:Context): Boolean {
        if (context != null) {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            val isAvailable=activeNetworkInfo != null && activeNetworkInfo.isConnected
            if(!isAvailable)
                UiUtil.showToast(context,"No internet connection")
            return isAvailable
        }
         return false
    }

    fun getAuthToken(): String? {
        return Preference.getString("token","")
    }

    fun isSuccessfulResponse(response:BaseResponse): Boolean {
        return response.responseCode==ResponseCode.OPERATION_SUCCESSFUL
    }
}