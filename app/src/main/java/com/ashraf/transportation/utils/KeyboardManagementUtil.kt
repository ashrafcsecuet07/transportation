package com.ashraf.transportation.utils

import android.app.Activity
import android.view.inputmethod.InputMethodManager

object KeyboardManagementUtil {
    fun hideSoftKeyboard(activity: Activity?) {
        try {
            val inputMethodManager = activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager?
            inputMethodManager?.hideSoftInputFromWindow(UiUtil.getActivityContentView(activity).getWindowToken(), 0)

        } catch (ex: Exception) {
            ///   ex.printStackTrace();
        }

    }
}