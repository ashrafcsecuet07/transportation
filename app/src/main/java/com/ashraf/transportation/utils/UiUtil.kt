package com.ashraf.transportation.utils

import android.app.Activity
import android.content.Context
import android.support.v4.content.ContextCompat
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast

object  UiUtil {
    fun  setVisibility(Visibility: Int, vararg viewList: View) {
        for (view in viewList) {
            view.visibility = Visibility
        }
    }

    fun getColor(context: Context, colorId: Int): Int {
        return ContextCompat.getColor(context, colorId)
    }


    fun showToast(context: Context?, toastMessage: String?) {
        if(!toastMessage.isNullOrEmpty())
            Toast.makeText(context, toastMessage, Toast.LENGTH_LONG).show()
    }

    fun getResourceString(context: Context, resourceId: Int): String {
        return context.getString(resourceId)
    }

    fun getResourceString(view: View, resourceId: Int): String {
        return getResourceString(view.context, resourceId)
    }

    fun getTextFieldValue(etText: TextView): String {
        return etText.text.toString().trim { it <= ' ' }
    }

    fun enableTouch(activity: Activity) {
        activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }
    //disable screen and back button interaction
    fun disbleTouch(activity: Activity) {
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }
    fun getActivityContentView(actvity:Activity?): View {
        return actvity?.getWindow()?.getDecorView()?.findViewById(android.R.id.content)!!

    }
}