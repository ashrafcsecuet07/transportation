package com.ashraf.transportation.utils

import android.support.design.widget.TextInputLayout

object FormValidationUtils {
    fun hideErrorMessage(textInputLayout: TextInputLayout) {
        textInputLayout.error = null
        textInputLayout.isErrorEnabled = false
    }

    fun showErrorMessage(textInputLayout: TextInputLayout, message: String?) {
        if (!textInputLayout.isErrorEnabled) {
            textInputLayout.error = message
        }
    }
}