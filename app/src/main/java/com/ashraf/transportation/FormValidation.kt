package com.ashraf.transportation

class FormValidation {
    var isValid  =false
    var errorMessage=""
    var errorMessageResourceId=0
    constructor(){
        this.isValid=true

    }
    constructor(errorMessage:String){
        this.errorMessage=errorMessage
        this.isValid=false
    }
    constructor(errorMessageResourceId:Int){
        this.errorMessageResourceId=errorMessageResourceId
        this.isValid=false
    }

}